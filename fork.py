try:
    from tornado.process import fork_processes
    fork_processes(0)
except:
    pass

try:
    import tornpack_settings
except ImportError:  
    print '''run make!'''   
    raise
except:
    raise 

import config
from tornpack.main import Main
from tornpack.options import options

try:
    assert options.tornpack_services

    for service in options.tornpack_services:
        exec "import %s" % service
except AssertionError:
    pass
except:
    raise

Main().init()
