from uuid import uuid4

class TornpackSettings(object):
    __file = None
    __parser = None

    @property
    def file(self):
        try:
            assert self.__file
        except AssertionError:
            self.__file = open('tornpack_settings.py','w')
        except:
            raise
        return self.__file

    @property
    def uuid(self):
        return uuid4().hex

    def __init__(self):
        self.routine()

    def routine(self):
        self.tornado_options()
        self.tornpack_name()
        self.tornpack_raid()
        self.file.close()
        return True

    def tornado_options(self):
        self.file.write('''from tornado.options import define,options\n''')
        return True

    def tornpack_name(self):
        self.file.write('''define("tornpack_name",default="%s")\n''' % self.uuid)
        return True

    def tornpack_raid(self):
        self.file.write('''define("tornpack_raid",default=%s)\n''' % False)
        self.file.write('''define("tornpack_raid_port",default=%s)\n''' % 8881)
        return True   

if __name__ == "__main__":
    TornpackSettings()
