import actor
import ioengine
import njord
import parser
import rabbitmq

tests = [
    'tests.actor.TestBase',
    'tests.ioengine.TestIOEngine',
    'tests.njord.mysql.TestCursorClose',
    'tests.njord.mysql.TestCursorFetch',
    'tests.njord.mysql.TestCursorHandler',
    'tests.njord.mysql.TestDelete',
    'tests.njord.mysql.TestInsert',
    'tests.njord.mysql.TestStatic',
    'tests.njord.mysql.TestSelect',
    'tests.njord.mysql.TestSelectOne',
    'tests.njord.mysql.TestUpdate',
    'tests.parser.Date',
    'tests.parser.JsonDictfy',
    'tests.parser.JsonJsonify',
    'tests.parser.JWT',
    'tests.parser.Protobuf',
    'tests.rabbitmq.TestKernel'
]
