from tornpack.ioengine import IOEngine
from tornpack.njord.mysql.exceptions import DecodeException,EncodeException,QueryInvalid
from tornpack.njord.mysql.cursor.close import Close
from tornpack.njord.mysql.static import mysql_path
from tornpack.parser.json import jsonify
from tornpack.testing import AsyncTestCase

__all__ = ['TestCursorClose']

class TestCursorClose(AsyncTestCase):
    def test_name(self):
        self.assertEquals('njord_mysql_cursor_close',Close().name)

    def test_service(self):
        self.assertEquals('cursor_close',Close().service)
