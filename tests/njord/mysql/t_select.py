from tornpack.ioengine import IOEngine
from tornpack.njord.mysql.exceptions import DecodeException,EncodeException,ExecutionError
from tornpack.njord.mysql.select import Select
from tornpack.njord.mysql.select.select_pb2 import Select as PBSelect
from tornpack.njord.mysql.static import mysql_path
from tornpack.parser.json import jsonify
from tornpack.testing import AsyncTestCase

__all__ = ['TestSelect']

class TestSelect(AsyncTestCase):
    def test__op_decode_error__(self):
        def on_call(result):
            IOEngine.ioloop.stop()
            self.assertRaises(DecodeException,result.result)

        Select().__op_decode_error__(None,IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__op_decode_error__without_body(self):
        self.assertRaises(TypeError,Select().__op_decode_error__,future=None)

    def test__op_decode_error__without_future(self):
        self.assertRaises(TypeError,Select().__op_decode_error__,body=None)

    def test__op_execution_error__(self):
        class proto:
            class error:
                code = 1234
                text = 'aaa'

        def on_call(result):
            IOEngine.ioloop.stop()
            self.assertRaises(ExecutionError,result.result)
            try:
                result.result()
            except ExecutionError as e:
                self.assertEquals(proto.error.code,e.args[0])
                self.assertEquals(proto.error.text,e.args[1])

        Select().__op_execution_error__(proto,IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__op_execution_error__without_body(self):
        self.assertRaises(TypeError,Select().__op_execution_error__,future=None)

    def test__op_execution_error__without_future(self):
        self.assertRaises(TypeError,Select().__op_execution_error__,body=None)

    def test__op_nok__(self):
        def on_call(result):
            IOEngine.ioloop.stop()
            self.assertFalse(result.result())

        Select().__op_nok__(None,IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__op_nok__without_body(self):
        self.assertRaises(TypeError,Select().__op_nok__,future=None)

    def test__op_nok__without_future(self):
        self.assertRaises(TypeError,Select().__op_nok__,body=None)

    def test__op_ok__(self):
        json = {'a':2}
        cursor = '12345'
        service = '2222'

        def on_call(result):
            IOEngine.ioloop.stop()
            self.assertEquals(json,result.result()['doc'])
            self.assertEquals(cursor,result.result()['cursor'].cursor)

        class proto():
            def __init__(self,**kwargs):
                for k in kwargs:
                    setattr(self,k,kwargs[k])

        Select().__op_ok__(
            proto(payload=jsonify(json),service=service,cursor=cursor),
            IOEngine.future_instance(on_call)
        )
        IOEngine.ioloop.start()

    def test__op_ok__without_body(self):
        self.assertRaises(TypeError,Select().__op_ok__,future=None)

    def test__op_ok__without_future(self):
        self.assertRaises(TypeError,Select().__op_ok__,body=None)

    def test__prepare_proto__(self):
        env = 'test'
        cond = 'id = 5'
        having = 'avg(id)'
        join = [{'db':'test','table':'dbtable2'}]
        fields = ['id','price']
        group = ['price']
        order = [{'name':'price'}]
        limit = 5
        proto = Select().__prepare_proto__(env,cond,having,join,fields,group,order,limit)
        self.assertEquals(env,proto.env)
        self.assertEquals(cond,proto.cond)
        self.assertEquals(having,proto.having)
        self.assertEquals(join[0]['db'],proto.join[0].db)
        self.assertEquals(join[0]['table'],proto.join[0].table)
        self.assertEquals(fields,proto.fields)
        self.assertEquals(group,proto.group)
        self.assertEquals(order[0]['name'],proto.order[0].name)
        self.assertEquals(limit,proto.limit)

    def test__prepare_proto__without_cond(self):
        self.assertRaises(TypeError,Select().__prepare_proto__,env=None,having=None,join=None,fields=None,group=None,order=None,limit=None)

    def test__prepare_proto__without_env(self):
        self.assertRaises(TypeError,Select().__prepare_proto__,cond=None,having=None,join=None,fields=None,group=None,order=None,limit=None)

    def test__prepare_proto__without_fields(self):
        self.assertRaises(TypeError,Select().__prepare_proto__,env=None,cond=None,having=None,join=None,group=None,order=None,limit=None)

    def test__prepare_proto__without_group(self):
        self.assertRaises(TypeError,Select().__prepare_proto__,env=None,cond=None,having=None,join=None,fields=None,order=None,limit=None)

    def test__prepare_proto__without_having(self):
        self.assertRaises(TypeError,Select().__prepare_proto__,env=None,cond=None,join=None,fields=None,group=None,order=None,limit=None)

    def test__prepare_proto__without_join(self):
        self.assertRaises(TypeError,Select().__prepare_proto__,env=None,cond=None,having=None,fields=None,group=None,order=None,limit=None)

    def test__prepare_proto__without_limit(self):
        self.assertRaises(TypeError,Select().__prepare_proto__,env=None,cond=None,having=None,join=None,fields=None,group=None,order=None)

    def test__prepare_proto__without_order(self):
        self.assertRaises(TypeError,Select().__prepare_proto__,env=None,cond=None,having=None,join=None,fields=None,group=None,limit=None)

    def test__prepare_proto__wrong_value(self):
        self.assertRaises(
            EncodeException,Select().__prepare_proto__,
            env='test',
            cond=5,
            having='avg(id)',
            join=[{'db':'test','table':'dbtable2'}],
            fields=['id','price'],
            group=['price'],
            order=[{'name':'price'}],
            limit=None
        )

    def test__proto_set_list__(self):
        proto = PBSelect()
        value = ['name','test']
        Select().__proto_set_list__('fields',value,proto)
        self.assertEquals(value,proto.fields)

    def test__proto_set_list__empty_list(self):
        proto = PBSelect()
        value = []
        Select().__proto_set_list__('fields',value,proto)
        self.assertEquals(value,proto.fields)

    def test__proto_set_list__without_k(self):
        self.assertRaises(TypeError,Select().__proto_set_list__,v=None,proto=None)

    def test__proto_set_list__without_v(self):
        self.assertRaises(TypeError,Select().__proto_set_list__,k=None,proto=None)

    def test__proto_set_list__without_proto(self):
        self.assertRaises(TypeError,Select().__proto_set_list__,k=None,v=None)

    def test__proto_set_list__wrong_value(self):
        proto = PBSelect()
        Select().__proto_set_list__('fields','231231',proto)
        self.assertEquals([],proto.fields)

    def test__proto_set_optional__(self):
        proto = PBSelect()
        value = 'id = 3'
        Select().__proto_set_optional__('cond',value,proto)
        self.assertEquals(value,proto.cond)

    def test__proto_set_optional__without_k(self):
        self.assertRaises(TypeError,Select().__proto_set_optional__,v=None,proto=None)

    def test__proto_set_optional__without_proto(self):
        self.assertRaises(TypeError,Select().__proto_set_optional__,k=None,v=None)

    def test__proto_set_optional__without_v(self):
        self.assertRaises(TypeError,Select().__proto_set_optional__,k=None,proto=None)

    def test__proto_set_optional__wrong_value(self):
        self.assertRaises(TypeError,Select().__proto_set_optional__,'cond',10,PBSelect())

    def test_mysql_path(self):
        obj = Select()
        self.assertEquals(obj.mysql_path('test','dbtest','dbtable'),mysql_path('test','dbtest','dbtable',obj.service))

    def test_mysql_path_without_db(self):
        self.assertRaises(TypeError,Select().mysql_path,env='test',table='dbtable')

    def test_mysql_path_without_env(self):
        self.assertRaises(TypeError,Select().mysql_path,db='dbtest',table='dbtable')

    def test_mysql_path_without_table(self):
        self.assertRaises(TypeError,Select().mysql_path,env='test',db='dbtest')

    def test_name(self):
        self.assertEquals('njord_mysql_select',Select().name)

    def test_parse_join_type_full(self):
        self.assertEquals(3,Select.parse_join_type('FULL'))

    def test_parse_join_type_inner(self):
        self.assertEquals(0,Select.parse_join_type('INNER'))

    def test_parse_join_type_left(self):
        self.assertEquals(1,Select.parse_join_type('LEFT'))

    def test_parse_join_type_non_existing(self):
        self.assertRaises(ValueError,Select.parse_join_type,'LOTTER')

    def test_parse_join_type_right(self):
        self.assertEquals(2,Select.parse_join_type('RIGHT'))

    def test_parse_order_direction_asc(self):
        self.assertEquals(0,Select.parse_order_direction('ASC'))

    def test_parse_order_direction_desc(self):
        self.assertEquals(1,Select.parse_order_direction('DESC'))

    def test_parse_order_direction_non_existing(self):
        self.assertRaises(ValueError,Select.parse_order_direction,'DASC')

    def test_service(self):
        self.assertEquals('select',Select().service)
