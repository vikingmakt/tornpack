from tornpack.ioengine import IOEngine
from tornpack.njord.mysql.exceptions import DecodeException,ExecutionError,QueryInvalid
from tornpack.njord.mysql.update import Update
from tornpack.testing import AsyncTestCase

__all__ = ['TestUpdate']

class TestUpdate(AsyncTestCase):
    def test__op_decode_error__(self):
        def on_call(result):
            IOEngine.ioloop.stop()
            self.assertRaises(DecodeException,result.result)

        Update().__op_decode_error__(None,IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__op_decode_error__without_body(self):
        self.assertRaises(TypeError,Update().__op_decode_error__,future=None)

    def test__op_decode_error__without_future(self):
        self.assertRaises(TypeError,Update().__op_decode_error__,body=None)

    def test__op_execution_error__(self):
        class proto:
            class error:
                code = 1234
                text = '123123'

        def on_call(result):
            IOEngine.ioloop.stop()
            x = self.assertRaises(ExecutionError,result.result)
            try:
                result.result()
            except Exception as ex:
                self.assertEquals(proto.error.code,ex.args[0])
                self.assertEquals(proto.error.text,ex.args[1])

        Update().__op_execution_error__(proto,IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__op_execution_error__without_body(self):
        self.assertRaises(TypeError,Update().__op_execution_error__,future=None)

    def test__op_execution_error__without_future(self):
        self.assertRaises(TypeError,Update().__op_execution_error__,body=None)

    def test__op_nok__(self):
        def on_call(result):
            self.assertFalse(result.result())
            IOEngine.ioloop.stop()

        Update().__op_nok__(None,IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__op_nok__without_body(self):
        self.assertRaises(TypeError,Update().__op_nok__,future=None)

    def test__op_nok__without_future(self):
        self.assertRaises(TypeError,Update().__op_nok__,body=None)

    def test__op_ok__(self):
        json = {'a':2}
        def on_call(result):
            self.assertTrue(result.result())
            IOEngine.ioloop.stop()

        Update().__op_ok__(None,IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__op_ok__without_body(self):
        self.assertRaises(TypeError,Update().__op_ok__,future=None)

    def test__op_ok__without_future(self):
        self.assertRaises(TypeError,Update().__op_ok__,body=None)

    def test_mysql_path_without_db(self):
        self.assertRaises(TypeError,Update().mysql_path,env='test',table='dbtable')

    def test_mysql_path_without_env(self):
        self.assertRaises(TypeError,Update().mysql_path,db='dbtest',table='dbtable')

    def test_mysql_path_without_table(self):
        self.assertRaises(TypeError,Update().mysql_path,env='test',db='dbtest')

    def test_name(self):
        self.assertEquals('njord_mysql_update',Update().name)

    def test_service(self):
        self.assertEquals('update',Update().service)
