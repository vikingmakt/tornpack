from calendar import timegm
from datetime import datetime
from tornpack.parser.date import datetime2timestamp, timestamp2datetime
from tornpack.testing import AsyncTestCase

__all__ = ['Date']

class Date(AsyncTestCase):
    __date = datetime.utcnow()

    def test_d2t(self):
        self.assertEquals(
            datetime2timestamp(self.__date),
            timegm(self.__date.timetuple()) + self.__date.microsecond/1e6
        )

    def test_t2d(self):
        self.assertEquals(
            timestamp2datetime(datetime2timestamp(self.__date)),
            datetime.fromtimestamp(int(datetime2timestamp(self.__date)))
        )
