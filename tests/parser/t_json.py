from tornpack.parser.json import dictfy, jsonify
from tornpack.testing import AsyncTestCase

__all__ = [
    'JsonDictfy',
    'JsonJsonify'
]

class JsonDictfy(AsyncTestCase):
    __dict = {"name":"Umgeher"}
    __json = """{"name":"Umgeher"}"""

    def test_dictfy_false(self):
        assert not dictfy(self.__dict)
    
    def test_dictfy_true(self):
        assert dictfy(self.__json) == self.__dict

class JsonJsonify(AsyncTestCase):
    __dict = {"name":"Umgeher"}
    __json = """{"name":"Umgeher"}"""

    def test_jsonify_true(self):
        assert jsonify(self.__dict) == self.__json

