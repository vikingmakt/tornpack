from base64 import b64decode,b64encode
from test_pb2 import PBTest
from tornpack.parser.protobuf import decode, encode
from tornpack.testing import AsyncTestCase

__all__ = ['Protobuf']

class Protobuf(AsyncTestCase):
    def test_decode(self):
        pb = PBTest(name="dht_node",value="some")
        target = PBTest()
        target.ParseFromString(b64decode(encode(pb)))

        self.assertEquals(
            decode(encode(pb),PBTest()),
            target
        )

    def test_decode_with_invalid_payload(self):
        self.assertFalse(decode('123123',PBTest()))

    def test_encode(self):
        pb = PBTest(name="dht_node",value="some")

        self.assertEquals(
            encode(pb),
            b64encode(pb.SerializeToString())
        )
