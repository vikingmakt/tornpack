from pika import BasicProperties
from tornpack.actor.rabbitmq import SimpleAsk
from tornpack.actor.rabbitmq.static import routing_key
from tornpack.options import options
from tornpack.parser.json import dictfy,jsonify

__all__ = ['Auth']

class Auth(SimpleAsk):
    @property
    def etag(self):
        try:
            return self.__etag
        except AttributeError:
            self.__etag = self.ioengine.uuid4
        except:
            raise
        return self.etag

    @property
    def name(self):
        return self.uid

    @property
    def tell(self):
        return self.__tell.set_result

    def __ask__(self):
        self.rabbitmq_channel.basic_publish(
            body=self.__payload,
            exchange=options.tornpack_rabbitmq_exchange['default'],
            routing_key=routing_key(options.tornpack_uprofile_rabbitmq['services']['acl']['get']),
            properties=BasicProperties(headers={'etag':self.etag})
        )
        return True

    def __init__(self,group,user,spec,future):
        try:
            assert options.tornpack_acl_ignore and options.debug
        except AssertionError:
            self.run(
                group=group,
                user=user,
                spec=spec,
                future=future
            )
            self.ioengine.ioloop.add_callback(self.__on_init__)
        except:
            raise
        else:
            future.set_result(True)

    def __rabbitmq_queue_bind_etag__(self):
        self.__rabbitmq_queue_bind__(
            queue=self.queue,
            exchange=options.tornpack_rabbitmq_exchange['headers'],
            routing_key='',
            arguments={
                'etag':self.etag,
                'code':options.tornpack_uprofile_rabbitmq['codes']['acl']['get']['ok']
            }
        )

        self.__rabbitmq_queue_bind__(
            queue=self.queue,
            exchange=options.tornpack_rabbitmq_exchange['headers'],
            routing_key='',
            arguments={
                'etag':self.etag,
                'code':options.tornpack_uprofile_rabbitmq['codes']['acl']['get']['nok']
            }
        )

        self.ioengine.ioloop.add_callback(self.__ask__)
        return True

    def make(self,**kwargs):
        self.ioengine.ioloop.add_callback(self.response_promise,msg=kwargs)
        return True

    def prepare_payload(self,**kwargs):
        kwargs = kwargs['msg']

        self.__payload = jsonify({
            'g':kwargs['group'],
            'k':kwargs['user']
        })

        self.ioengine.ioloop.add_callback(self.__rabbitmq_connection__)
        return True

    def response_promise(self,**kwargs):
        kwargs = kwargs['msg']

        def on_tell(result):
            try:
                result = dictfy(result.result()['body'])
                assert result
                assert result['v'] == 'w' or result['v'] == kwargs['spec']
            except (AssertionError,KeyError):
                kwargs['future'].set_result(False)
            except:
                raise
            else:
                kwargs['future'].set_result(True)
            return True

        self.__tell = self.ioengine.future_instance(on_tell)
        self.ioengine.ioloop.add_callback(self.prepare_payload,msg=kwargs)
        return True
