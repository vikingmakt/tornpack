from tornpack.actor.base import Base

__all__ = ['Kernel']

class Kernel(Base):
    __connections = None

    @property
    def connections(self):
        try:
            assert self.__connections
        except AssertionError:
            self.__connections = {}
        except:
            raise
        return self.__connections

    def connection_add(self,io):
        self.connections[io.etag] = io
        return True

    def connection_del(self,io):
        try:
            assert io.etag in self.connections
        except AssertionError:
            pass
        except:
            raise
        else:
            del self.connections[io.etag]
        return True

Kernel = Kernel()
