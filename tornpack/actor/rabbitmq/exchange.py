from tornpack.actor.base import Base

__all__ = ['Exchange']

class Exchange(Base):
    def __on_rabbitmq_exchange_bind__(self,destination,source,routing_key,arguments):
        pass

    def __on_rabbitmq_exchange_declare__(self,exchange):
        pass

    def __on_rabbitmq_exchange_delete__(self,exchange):
        pass

    def __on_rabbitmq_exchange_unbind__(self,destination,source,routing_key,nowait,arguments):
        pass

    def __rabbitmq_exchange_bind(self,channel,destination,source,routing_key,arguments,future):
        def on_bind(result):
            future.set_result(True)
            self.ioengine.ioloop.add_callback(
                self.__on_rabbitmq_exchange_bind__,
                destination=destination,
                source=source,
                routing_key=routing_key,
                arguments=arguments
            )
            return True

        channel.exchange_bind(
            destination=destination,
            source=source,
            routing_key=routing_key,
            arguments=arguments,
            callback=on_bind
        )
        return True

    def __rabbitmq_exchange_declare(self,channel,exchange,exchange_type,future,passive,durable,auto_delete,internal,arguments):
        def on_declare(result):
            future.set_result(True)
            self.ioengine.ioloop.add_callback(self.__on_rabbitmq_exchange_declare__,exchange=exchange)
            return True

        channel.exchange_declare(
            exchange=exchange,
            exchange_type=exchange_type,
            passive=passive,
            durable=durable,
            auto_delete=auto_delete,
            internal=internal,
            arguments=arguments,
            callback=on_declare
        )
        return True

    def __rabbitmq_exchange_delete(self,channel,exchange,future,if_unused,nowait):
        def on_delete(result):
            future.set_result(True)
            self.ioengine.ioloop.add_callback(self.__on_rabbitmq_exchange_delete__,exchange=exchange)
            return True

        channel.exchange_delete(
            callback=on_delete,
            exchange=exchange,
            if_unused=if_unused,
            nowait=nowait
        )
        return True

    def __rabbitmq_exchange_unbind(self,channel,destination,source,future,routing_key,nowait,arguments):
        def on_unbind(result):
            future.set_result(True)
            self.ioengine.ioloop.add_callback(
                self.__on_rabbitmq_exchange_unbind__,
                destination=destination,
                source=source,
                routing_key=routing_key,
                nowait=nowait,
                arguments=arguments
            )
            return True

        channel.exchange_unbind(
            destination=destination,
            source=source,
            routing_key=routing_key,
            nowait=nowait,
            arguments=arguments
        )
        return True

    def __rabbitmq_exchange_bind__(self,destination,source,routing_key,arguments=None,future=None):
        def on_bind(result):
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':True}})
            return True

        try:
            assert self.rabbitmq_channel
        except:
            raise
        else:
            self.__rabbitmq_exchange_bind(
                channel=self.rabbitmq_channel,
                destination=destination,
                source=source,
                routing_key=routing_key,
                arguments=arguments,
                future=self.ioengine.future_instance(on_bind)
            )
        return True

    def __rabbitmq_exchange_declare__(self,exchange,exchange_type,future=None,passive=False,durable=False,auto_delete=False,internal=False,arguments=None):
        def on_declare(result):
            try:
                future.set_result(True)
            except AttributeError:
                pass
            except:
                raise
            return True

        try:
            assert self.rabbitmq_channel
        except:
            raise
        else:
            self.__rabbitmq_exchange_declare(
                channel=self.rabbitmq_channel,
                exchange=exchange,
                exchange_type=exchange_type,
                future=self.ioengine.future_instance(on_declare),
                passive=passive,
                durable=durable,
                auto_delete=auto_delete,
                internal=internal,
                arguments=arguments
            )
        return True

    def __rabbitmq_exchange_delete__(self,exchange,future=None,if_unused=False,nowait=False):
        def on_delete(result):
            try:
                future.set_result(True)
            except AttributeError:
                pass
            except:
                raise
            return True

        try:
            assert self.rabbitmq_channel
        except:
            raise
        else:
            self.__rabbitmq_exchange_delete(
                channel=self.rabbitmq_channel,
                exchange=exchange,
                future=self.ioengine.future_instance(on_delete),
                if_unused=if_unused,
                nowait=nowait
            )
        return True

    def __rabbitmq_exchange_unbind__(self,destination=None,source=None,future=None,routing_key='',nowait=False,arguments=None):
        def on_unbind(result):
            try:
                future.set_result(True)
            except AttributeError:
                pass
            except:
                raise
            return True

        try:
            assert self.rabbitmq_channel
        except:
            raise
        else:
            self.__rabbitmq_exchange_unbind(
                destination=destination,
                source=source,
                future=self.ioengine.future_instance(on_unbind),
                routing_key=routing_key,
                nowait=nowait,
                arguments=arguments
            )
        return True
