from tornpack.actor.base import Base
import static

__all__ = ['Queue']

class Queue(Base):
    def __rabbitmq_queue_bind(self,channel,queue,exchange,routing_key,arguments,future):
        def on_bind(result):
            self.ioengine.ioloop.add_callback(
                self.__on_rabbitmq_queue_bind__,
                queue=queue,
                exchange=exchange,
                routing_key=routing_key
            )
            future.set_result(True)
            return True

        channel.queue_bind(
            queue=queue,
            exchange=exchange,
            routing_key=routing_key,
            arguments=arguments,
            callback=on_bind
        )
        return True

    def __rabbitmq_queue_declare(self,channel,queue,passive,durable,exclusive,auto_delete,nowait,arguments,future):
        def on_declare(result):
            self.ioengine.ioloop.add_callback(self.__on_rabbitmq_queue_declare__,queue=queue)
            future.set_result(True)
            return True

        channel.queue_declare(
            callback=on_declare,
            queue=queue,
            passive=passive,
            durable=durable,
            exclusive=exclusive,
            auto_delete=auto_delete,
            nowait=nowait,
            arguments=arguments
        )
        return True

    def __rabbitmq_queue_unbind(self,channel,queue,exchange,routing_key,arguments,future):
        def on_unbind(result):
            self.ioengine.ioloop.add_callback(
                self.__on_rabbitmq_queue_unbind__,
                queue=queue,
                exchange=exchange,
                routing_key=routing_key
            )
            future.set_result(True)
            return True

        channel.queue_unbind(
            queue=queue,
            exchange=exchange,
            routing_key=routing_key,
            arguments=arguments,
            callback=on_unbind
        )
        return True

    def __on_rabbitmq_queue_bind__(self,queue,exchange,routing_key):
        pass

    def __on_rabbitmq_queue_declare__(self,queue):
        pass

    def __on_rabbitmq_queue_unbind__(self,queue,exchange,routing_key):
        pass

    def __rabbitmq_queue_bind__(self,queue,exchange,routing_key,arguments=None,future=None):
        def on_bind(result):
            try:
                future.set_result(True)
            except AttributeError:
                pass
            except:
                raise
            return True


        self.__rabbitmq_queue_bind(
            channel=self.rabbitmq_channel,
            queue=queue,
            exchange=exchange,
            routing_key=routing_key,
            arguments=arguments,
            future=self.ioengine.future_instance(on_bind)
        )
        return True

    def __rabbitmq_queue_declare__(self,queue,passive=False,durable=False,exclusive=False,auto_delete=False,nowait=False,arguments=None,future=None):
        def on_declare(result):
            try:
                future.set_result(True)
            except AttributeError:
                pass
            except:
                raise
            return True

        self.__rabbitmq_queue_declare(
            channel=self.rabbitmq_channel,
            queue=queue,
            passive=passive,
            durable=durable,
            exclusive=exclusive,
            auto_delete=auto_delete,
            nowait=nowait,
            arguments=arguments,
            future=self.ioengine.future_instance(on_declare)
        )
        return True

    def __rabbitmq_queue_name__(self,_):
        return static.queue_name(_)

    def __rabbitmq_queue_unbind__(self,queue,exchange,routing_key,arguments=None,future=None):
        def on_unbind(result):
            try:
                future.set_result(True)
            except AttributeError:
                pass
            except:
                raise
            return True

        self.__rabbitmq_queue_unbind(
            channel=self.rabbitmq_channel,
            queue=queue,
            exchange=exchange,
            routing_key=routing_key,
            arguments=arguments,
            future=self.ioengine.future_instance(on_unbind)
        )
        return True

    def __rabbitmq_routing_key__(self,_):
        return static.routing_key(_)
