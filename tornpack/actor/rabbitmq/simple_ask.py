from .channel import Channel
from .connection import Connection
from .exchange import Exchange
from hashlib import sha1
from pika import BasicProperties
from .queue import Queue
from tornpack.options import options

__all__ = ['SimpleAsk']

class SimpleAsk(Channel,Connection,Exchange,Queue):
    __consumer_tag = None
    __rabbitmq_channel_prefetch_count = None
    __rabbitmq_queue_arguments = None
    __futures = None
    __uid = None

    @property
    def consumer_tag(self):
        return self.__consumer_tag

    @property
    def etag(self):
        return sha1('<%s>::%s::%s' % (self.name,self.ioengine.uuid4,self.ioengine.uuid4)).hexdigest()

    @property
    def name(self):
        return options.tornpack_actor_rabbitmq['simple_ask']['name']

    @property
    def queue(self):
        return self.__rabbitmq_queue_name__('ask:%s:%s' % (self.name,self.uid))

    @property
    def uid(self):
        try:
            assert self.__uid
        except AssertionError:
            self.__uid = self.ioengine.uuid4
        except:
            raise
        return self.__uid

    def __payload(self,msg):
        msg['payload']['properties'].headers['etag'] = msg['etag']
        msg['payload']['properties'].headers['__simple_ask__'] = True

        self.rabbitmq_channel.basic_publish(
            **msg['payload']
        )
        return True

    def __tell(self,etag,msg):
        try:
            self.__futures.pop(etag).set_result(msg)
        except (KeyError,AttributeError):
            pass
        except:
            raise
        return True

    def __ask__(self,future,body='',exchange=None,routing_key='',headers={},*args,**kwargs):
        msg = {
            'payload':{
                'body':body,
                'exchange':exchange or options.tornpack_rabbitmq_exchange['default'],
                'routing_key':routing_key,
                'properties':BasicProperties(headers=headers)
            },
            'etag':self.etag
        }
        self.__futures[msg['etag']] = future

        try:
            assert self.is_ready
        except AssertionError:
            def on_ready(result):
                self.ioengine.ioloop.add_callback(self.__rabbitmq_queue_bind_etag__,msg=msg)
                return True

            self.promises.append(self.ioengine.future_instance(on_ready))
        else:
            self.ioengine.ioloop.add_callback(self.__rabbitmq_queue_bind_etag__,msg=msg)
        return True

    def __init__(self):
        self.__futures = {}
        self.ioengine.ioloop.add_callback(self.__rabbitmq_connection__)
        self.ioengine.ioloop.add_callback(self.__on_init__)

    def __on_init__(self):
        pass

    def __on_rabbitmq_channel__(self,channel):
        self.ioengine.ioloop.add_callback(self.__rabbitmq_exchange_declare_default__)
        self.ioengine.ioloop.add_callback(self.__rabbitmq_exchange_declare_headers_any__)
        self.ioengine.ioloop.add_callback(self.__rabbitmq_queue_declare_default__)
        return True

    def __on_rabbitmq_connection__(self,connection):
        self.ioengine.ioloop.add_callback(self.__rabbitmq_channel__)
        return True

    def __rabbitmq_consumer__(self):
        def on_msg(channel,deliver,properties,body):
            channel.basic_ack(deliver.delivery_tag)
            try:
                assert properties.headers['__simple_ask__']
            except (AssertionError,KeyError):
                self.__tell(properties.headers['etag'],{
                    'channel':channel,
                    'deliver':deliver,
                    'properties':properties,
                    'body':body
                })
                self.ioengine.ioloop.add_callback(self.__rabbitmq_queue_unbind_etag__,properties.headers['etag'])
            except:
                raise
            return True

        self.__consumer_tag = self.rabbitmq_channel.basic_consume(
            consumer_callback=on_msg,
            queue=self.queue
        )
        return True

    def __rabbitmq_exchange_declare_default__(self):
        def on_declare(result):
            return True

        self.__rabbitmq_exchange_declare__(
            exchange=options.tornpack_rabbitmq_exchange['default'],
            exchange_type='topic',
            future=self.ioengine.future_instance(on_declare),
            durable=True
        )
        return True

    def __rabbitmq_exchange_declare_headers_any__(self):
        def on_declare(result):
            self.ioengine.ioloop.add_callback(self.__rabbitmq_queue_bind_uid__)
            return True

        self.__rabbitmq_exchange_declare__(
            exchange=options.tornpack_rabbitmq_exchange['headers_any'],
            exchange_type='headers',
            future=self.ioengine.future_instance(on_declare),
            durable=True,
            arguments={
                'x-match':'any',
                'alternate-exchange':options.tornpack_rabbitmq_exchange['headers_any_ae']
            }
        )
        return True

    def __rabbitmq_queue_bind_etag__(self,msg):
        def on_bind(result):
            self.ioengine.ioloop.add_callback(self.__payload,msg=msg)
            return True

        self.__rabbitmq_queue_bind__(
            queue=self.queue,
            exchange=options.tornpack_rabbitmq_exchange['headers_any'],
            routing_key='',
            arguments={
                'etag':msg['etag']
            },
            future=self.ioengine.future_instance(on_bind)
        )
        return True

    def __rabbitmq_queue_bind_exchange__(self):
        self.ioengine.ioloop.add_callback(self.__rabbitmq_queue_bind_exchange_topic__)
        return True

    def __rabbitmq_queue_bind_exchange_topic__(self):
        def on_bind(result):
            self.ioengine.ioloop.add_callback(self.__rabbitmq_consumer__)
            return True

        self.__rabbitmq_queue_bind__(
            queue=self.queue,
            exchange=options.tornpack_rabbitmq_exchange['default'],
            routing_key=self.__rabbitmq_routing_key__(self.name),
            future=self.ioengine.future_instance(on_bind)
        )
        return True

    def __rabbitmq_queue_bind_uid__(self):
        def on_bind(result):
            self.is_ready = True
            return True

        self.__rabbitmq_queue_bind__(
            queue=self.queue,
            exchange=options.tornpack_rabbitmq_exchange['headers_any'],
            routing_key='',
            arguments={
                'uid':self.uid
            },
            future=self.ioengine.future_instance(on_bind)
        )
        return True

    def __rabbitmq_queue_declare_default__(self):
        def on_declare(result):
            self.ioengine.ioloop.add_callback(self.__rabbitmq_queue_bind_exchange__)
            return True

        self.__rabbitmq_queue_declare__(
            queue=self.queue,
            future=self.ioengine.future_instance(on_declare),
            exclusive=True,
            auto_delete=True
        )
        return True

    def __rabbitmq_queue_unbind_etag__(self,etag):
        self.__rabbitmq_queue_unbind__(
            queue=self.queue,
            exchange=options.tornpack_rabbitmq_exchange['headers_any'],
            routing_key='',
            arguments={'etag':etag}
        )
        return True
