from .channel import Channel
from .connection import Connection
from .exchange import Exchange
from .queue import Queue
from tornpack.options import options

__all__ = ['SimpleConsumer']

class SimpleConsumer(Channel,Connection,Exchange,Queue):
    __name = None
    __rabbitmq_channel_prefetch_count = None
    __rabbitmq_queue_arguments = None

    @property
    def name(self):
        return self.__name

    @property
    def queue(self):
        return self.__rabbitmq_queue_name__(self.name)

    @property
    def rabbitmq_queue_arguments(self):
        try:
            assert self.__rabbitmq_queue_arguments
        except AssertionError:
            self.__rabbitmq_queue_arguments = {'exclusive':False,'durable':True}
        except:
            raise
        return self.__rabbitmq_queue_arguments

    def __init__(self,name,prefetch_count=None,queue_arguments=None):
        self.__name = name
        self.__rabbitmq_channel_prefetch_count = prefetch_count
        self.__rabbitmq_queue_arguments = queue_arguments
        self.ioengine.ioloop.add_callback(self.__on_init__)
        self.ioengine.ioloop.add_callback(self.__rabbitmq_connection__)

    def __on_init__(self):
        pass

    def __on_rabbitmq_channel__(self,channel):
        self.ioengine.ioloop.add_callback(self.__rabbitmq_exchange_declare_default__)
        self.ioengine.ioloop.add_callback(self.__rabbitmq_exchange_declare_headers_ae__)
        self.ioengine.ioloop.add_callback(self.__rabbitmq_exchange_declare_headers__)
        self.ioengine.ioloop.add_callback(self.__rabbitmq_exchange_declare_headers_any_ae__)
        self.ioengine.ioloop.add_callback(self.__rabbitmq_exchange_declare_headers_any__)
        self.ioengine.ioloop.add_callback(self.__rabbitmq_queue_declare_default__)
        return True

    def __on_rabbitmq_connection__(self,connection):
        connection.on_close(self.ioengine.future_instance(self.__on_rabbitmq_connection_closed__))
        self.ioengine.ioloop.add_callback(self.__rabbitmq_channel__)
        return True

    def __on_rabbitmq_connection_closed__(self,connection):
        self.ioengine.ioloop.add_callback(self.__rabbitmq_connection__)
        return True

    def __rabbitmq_consumer_set_on_default__(self):
        def on_msg(channel,deliver,properties,body):
            def ack(result):
                try:
                    assert result.result()
                except AssertionError:
                    channel.basic_nack(deliver.delivery_tag)
                except:
                    raise
                else:
                    channel.basic_ack(deliver.delivery_tag)
                return True

            self.ioengine.ioloop.add_callback(
                self.on_message,
                msg={
                    'channel':channel,
                    'deliver':deliver,
                    'properties':properties,
                    'body':body,
                    'ack':self.ioengine.future_instance(ack)
                }
            )
            return True

        self.rabbitmq_channel.basic_consume(
            consumer_callback=on_msg,
            queue=self.queue
        )
        return True

    def __rabbitmq_exchange_bind_default_to_headers__(self):
        def on_bind(result):
            return True

        self.__rabbitmq_exchange_bind__(
            destination=options.tornpack_rabbitmq_exchange['headers'],
            source=options.tornpack_rabbitmq_exchange['default'],
            routing_key='#',
            future=self.ioengine.future_instance(on_bind)
        )
        return True

    def __rabbitmq_exchange_bind_default_to_headers_any__(self):
        def on_bind(result):
            return True

        self.__rabbitmq_exchange_bind__(
            destination=options.tornpack_rabbitmq_exchange['headers_any'],
            source=options.tornpack_rabbitmq_exchange['default'],
            routing_key='#',
            future=self.ioengine.future_instance(on_bind)
        )
        return True

    def __rabbitmq_exchange_declare_default__(self):
        def on_declare(result):
            self.ioengine.ioloop.add_callback(self.__rabbitmq_exchange_bind_default_to_headers__)
            self.ioengine.ioloop.add_callback(self.__rabbitmq_exchange_bind_default_to_headers_any__)
            return True

        self.__rabbitmq_exchange_declare__(
            exchange=options.tornpack_rabbitmq_exchange['default'],
            exchange_type='topic',
            future=self.ioengine.future_instance(on_declare),
            durable=True
        )
        return True

    def __rabbitmq_exchange_declare_headers__(self):
        def on_declare(result):
            return True

        self.__rabbitmq_exchange_declare__(
            exchange=options.tornpack_rabbitmq_exchange['headers'],
            exchange_type='headers',
            future=self.ioengine.future_instance(on_declare),
            durable=True,
            arguments={
                'x-match':'all',
                'alternate-exchange':options.tornpack_rabbitmq_exchange['headers_ae']
            }
        )
        return True

    def __rabbitmq_exchange_declare_headers_ae__(self):
        def on_declare(result):
            return True

        self.__rabbitmq_exchange_declare__(
            exchange=options.tornpack_rabbitmq_exchange['headers_ae'],
            exchange_type='headers',
            future=self.ioengine.future_instance(on_declare),
            durable=True,
            arguments={'x-match':'all'}
        )
        return True

    def __rabbitmq_exchange_declare_headers_any__(self):
        def on_declare(result):
            return True

        self.__rabbitmq_exchange_declare__(
            exchange=options.tornpack_rabbitmq_exchange['headers_any'],
            exchange_type='headers',
            future=self.ioengine.future_instance(on_declare),
            durable=True,
            arguments={
                'x-match':'any',
                'alternate-exchange':options.tornpack_rabbitmq_exchange['headers_any_ae']
            }
        )
        return True

    def __rabbitmq_exchange_declare_headers_any_ae__(self):
        def on_declare(result):
            return True

        self.__rabbitmq_exchange_declare__(
            exchange=options.tornpack_rabbitmq_exchange['headers_any_ae'],
            exchange_type='headers',
            future=self.ioengine.future_instance(on_declare),
            durable=True,
            arguments={
                'x-match':'any'
            }
        )
        return True

    def __rabbitmq_queue_bind_exchange__(self):
        self.ioengine.ioloop.add_callback(self.__rabbitmq_queue_bind_exchange_topic__)
        return True

    def __rabbitmq_queue_bind_exchange_topic__(self):
        def on_bind(result):
            self.ioengine.ioloop.add_callback(self.__rabbitmq_consumer_set_on_default__)
            return True

        self.__rabbitmq_queue_bind__(
            queue=self.queue,
            exchange=options.tornpack_rabbitmq_exchange['default'],
            routing_key=self.__rabbitmq_routing_key__(self.name),
            future=self.ioengine.future_instance(on_bind)
        )
        return True

    def __rabbitmq_queue_declare_default__(self):
        def on_declare(result):
            self.ioengine.ioloop.add_callback(self.__rabbitmq_queue_bind_exchange__)
            return True

        self.__rabbitmq_queue_declare__(
            queue=self.queue,
            future=self.ioengine.future_instance(on_declare),
            **self.rabbitmq_queue_arguments
        )
        return True

    def on_message(self,msg):
        pass
