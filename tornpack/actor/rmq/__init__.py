from hashlib import sha1
from pika.spec import BasicProperties
import static
from tornpack.actor.base import Base
from tornpack.options import options
from tornpack.rabbitmq import RabbitMQ

__all__ = ['RMQ']

class RMQ(Base):
    __name = None
    __rabbitmq_channel = None
    __rabbitmq_channel_prefetch_count = None
    __rabbitmq_connection = None

    @property
    def name(self):
        return self.__name

    @property
    def name_hash(self):
        return static.name_hash(self.name)

    @property
    def queue(self):
        return static.queue(self.name)

    @property
    def rabbitmq(self):
        return self.__rabbitmq_connection

    @property
    def rabbitmq_channel(self):
        return self.__rabbitmq_channel

    @property
    def rabbitmq_channel_name(self):
        return static.channel_name(self.name,self.uid)

    @property
    def rabbitmq_queue_arguments(self):
        try:
            assert self.__rabbitmq_queue_arguments
        except AssertionError:
            self.__rabbitmq_queue_arguments = {'exclusive':False,'auto_delete':False,'durable':True}
        except:
            raise
        return self.__rabbitmq_queue_arguments

    @property
    def routing_key(self):
        return static.routing_key(self.name)

    def __init__(self,name,channel_prefetch_count=options.tornpack_rabbitmq_channel_prefetch_count,queue_arguments=None):
        self.__rabbitmq_channel_prefetch_count = channel_prefetch_count
        self.__rabbitmq_queue_arguments = queue_arguments
        self.__name = name
        self.ioengine.ioloop.add_callback(self.__on_init__)

    def __on_init__(self):
        pass

    def __rabbitmq(self,future):
        def on_connection(result):
            self.__rabbitmq_connection = result.result()
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':future}})
            return True

        RabbitMQ.engine(options.tornpack_rabbitmq_name,self.ioengine.future_instance(on_connection))
        return True

    def __rabbitmq_broadcast__(self,channel,payload,headers={},future=None):
        try:
            channel.basic_publish(
                exchange=options.tornpack_rabbitmq_exchange['default'],
                routing_key='',
                body=payload,
                properties=BasicProperties(headers=headers)
            )
        except:
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':False}})
            raise
        else:
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':True}})
        return True

    def __rabbitmq_channel(self,future):
        def on_channel(channel):
            try:
                assert channel.result()
            except:
                self.__rabbitmq_channel = None
                self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':False}})
                self.ioengine.ioloop.add_callback(self.__rabbitmq_channel_error__)
                raise
            else:
                self.__rabbitmq_channel = channel.result()
                self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':True}})
                self.ioengine.ioloop.add_callback(self.__rabbitmq_channel_on__)
            return True

        self.rabbitmq.channel(
            self.rabbitmq_channel_name,
            future=self.ioengine.future_instance(on_channel),
            prefetch_count=self.__rabbitmq_channel_prefetch_count
        )
        return True

    def __rabbitmq_channel_error__(self):
        pass

    def __rabbitmq_channel_on__(self):
        pass

    def __rabbitmq_consumer(self):
        def on_message(channel,deliver,properties,body):
            def ack(result):
                try:
                    assert result.result()
                except AssertionError:
                    channel.basic_nack(deliver.delivery_tag)
                except:
                    raise
                else:
                    channel.basic_ack(deliver.delivery_tag)
                return True

            self.ioengine.ioloop.add_callback(
                self.on_message,
                msg={
                    'channel':channel,
                    'deliver':deliver,
                    'properties':properties,
                    'body':body,
                    'ack':self.ioengine.future_instance(ack)
                }
            )
            return True

        self.rabbitmq.consumer.consume(
            channel=self.__rabbitmq_channel,
            consumer_callback=on_message,
            queue=self.queue
        )
        return True

    def __rabbitmq_exchange_headers(self,future):
        def on_declare(result):
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':True}})
            self.ioengine.ioloop.add_callback(self.__rabbitmq_exchange_headers_on__)
            return True

        self.rabbitmq.exchange.declare(
            channel=self.__rabbitmq_channel,
            exchange=options.tornpack_rabbitmq_exchange['headers'],
            exchange_type='headers',
            durable=True,
            arguments={
                'x-match':'all',
                'alternate-exchange':options.tornpack_rabbitmq_exchange['headers_ae']
            },
            future=self.ioengine.future_instance(on_declare)
        )
        return True

    def __rabbitmq_exchange_headers_ae(self):
        def on_declare(result):
            self.ioengine.ioloop.add_callback(self.__rabbitmq_exchange_headers_ae_on__)
            return True

        self.rabbitmq.exchange.declare(
            channel=self.__rabbitmq_channel,
            exchange=options.tornpack_rabbitmq_exchange['headers_ae'],
            exchange_type='headers',
            durable=True,
            arguments={
                'x-match':'all'
            },
            future=self.ioengine.future_instance(on_declare)
        )
        return True

    def __rabbitmq_exchange_headers_any(self,future):
        def on_declare(result):
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':True}})
            self.ioengine.ioloop.add_callback(self.__rabbitmq_exchange_headers_any_on__)
            return True

        self.rabbitmq.exchange.declare(
            channel=self.__rabbitmq_channel,
            exchange=options.tornpack_rabbitmq_exchange['headers_any'],
            exchange_type='headers',
            durable=True,
            arguments={
                'alternate-exchange':options.tornpack_rabbitmq_exchange['headers_any_ae']
            },
            future=self.ioengine.future_instance(on_declare)
        )
        return True

    def __rabbitmq_exchange_headers_any_ae(self):
        def on_declare(result):
            self.ioengine.ioloop.add_callback(self.__rabbitmq_exchange_headers_any_ae_on__)
            return True

        self.rabbitmq.exchange.declare(
            channel=self.__rabbitmq_channel,
            exchange=options.tornpack_rabbitmq_exchange['headers_any_ae'],
            exchange_type='headers',
            durable=True,
            future=self.ioengine.future_instance(on_declare)
        )
        return True

    def __rabbitmq_exchange_headers_any_ae_on__(self):
        pass

    def __rabbitmq_exchange_headers_any_on__(self):
        pass

    def __rabbitmq_exchange_headers_ae_on__(self):
        pass

    def __rabbitmq_exchange_headers_on__(self):
        pass

    def __rabbitmq_exchange_topic(self,future):
        def on_declare(result):
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':True}})
            self.ioengine.ioloop.add_callback(self.__rabbitmq_exchange_topic_on__)
            return True

        self.rabbitmq.exchange.declare(
            channel=self.__rabbitmq_channel,
            exchange=options.tornpack_rabbitmq_exchange['default'],
            exchange_type='topic',
            durable=True,
            future=self.ioengine.future_instance(on_declare)
        )
        return True

    def __rabbitmq_exchange_topic_bind_to_headers(self,future):
        def on_bind(result):
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':True}})
            self.ioengine.ioloop.add_callback(self.__rabbitmq_exchange_topic_bind_to_headers_on__)
            return True

        self.rabbitmq.exchange.bind(
            channel=self.__rabbitmq_channel,
            destination=options.tornpack_rabbitmq_exchange['headers'],
            source=options.tornpack_rabbitmq_exchange['default'],
            routing_key='#',
            future=self.ioengine.future_instance(on_bind)
        )
        return True

    def __rabbitmq_exchange_topic_bind_to_headers_any(self,future):
        def on_bind(result):
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':True}})
            self.ioengine.ioloop.add_callback(self.__rabbitmq_exchange_topic_bind_to_headers_any_on__)
            return True

        self.rabbitmq.exchange.bind(
            channel=self.__rabbitmq_channel,
            destination=options.tornpack_rabbitmq_exchange['headers_any'],
            source=options.tornpack_rabbitmq_exchange['default'],
            routing_key='#',
            future=self.ioengine.future_instance(on_bind)
        )
        return True

    def __rabbitmq_exchange_topic_bind_to_headers_any_on__(self):
        pass

    def __rabbitmq_exchange_topic_bind_to_headers_on__(self):
        pass

    def __rabbitmq_exchange_topic_on__(self):
        pass

    def __rabbitmq_queue_bind_to_exchange_topic(self,future):
        def on_bind(result):
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':True}})
            self.ioengine.ioloop.add_callback(self.__rabbitmq_queue_bind_to_exchange_topic_on__)
            return True

        self.rabbitmq.queue.bind(
            channel=self.__rabbitmq_channel,
            queue=self.queue,
            exchange=options.tornpack_rabbitmq_exchange['default'],
            routing_key=self.routing_key,
            future=self.ioengine.future_instance(on_bind)
        )
        return True

    def __rabbitmq_queue_bind_to_exchange_topic_on__(self):
        pass

    def __rabbitmq_queue(self,future):
        def on_declare(result):
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':True}})
            self.ioengine.ioloop.add_callback(self.__rabbitmq_queue_on__)
            return True

        self.rabbitmq.queue.declare(
            channel=self.__rabbitmq_channel,
            queue=self.queue,
            future=self.ioengine.future_instance(on_declare),
            **self.rabbitmq_queue_arguments
        )
        return True

    def __rabbitmq_queue_on__(self):
        pass

    def __rabbitmq_publish__(self,body,future,exchange=options.tornpack_rabbitmq_exchange['default'],routing_key='',properties=BasicProperties(headers={})):
        try:
            self.rabbitmq_channel.basic_publish(
                exchange=exchange,
                routing_key=routing_key,
                body=body,
                properties=properties
            )
        except:
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':False}})
            raise
        else:
            self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':True}})
        return True

    def on_message(self,msg):
        pass

    def start(self):
        def on_rabbitmq(result):
            self.ioengine.ioloop.add_callback(self.start)
            return True

        def on_queue_bind_to_exchange_topic(result):
            self.ioengine.ioloop.add_callback(self.__rabbitmq_consumer)
            self.is_ready = True
            return True

        def on_queue(result):
            self.ioengine.ioloop.add_callback(
                self.__rabbitmq_queue_bind_to_exchange_topic,
                future=self.ioengine.future_instance(on_queue_bind_to_exchange_topic)
            )
            return True

        def on_exchange_topic_bind_to_headers(result):
            self.ioengine.ioloop.add_callback(
                self.__rabbitmq_queue,
                future=self.ioengine.future_instance(on_queue)
            )
            return True

        def on_exchange_topic_bind_to_headers_any(result):
            return True

        def on_exchange_headers_any(result):
            self.ioengine.ioloop.add_callback(
                self.__rabbitmq_exchange_topic_bind_to_headers_any,
                future=self.ioengine.future_instance(on_exchange_topic_bind_to_headers_any)
            )
            return True

        def on_exchange_headers(result):
            return True

        def on_exchange_topic(result):
            self.ioengine.ioloop.add_callback(
                self.__rabbitmq_exchange_topic_bind_to_headers,
                future=self.ioengine.future_instance(on_exchange_topic_bind_to_headers)
            )
            return True

        def on_channel(result):
            try:
                assert result.result()
            except AssertionError:
                pass
            except:
                raise
            else:
                self.ioengine.ioloop.add_callback(
                    self.__rabbitmq_exchange_headers,
                    future=self.ioengine.future_instance(on_exchange_headers)
                )
                self.ioengine.ioloop.add_callback(
                    self.__rabbitmq_exchange_headers_any,
                    future=self.ioengine.future_instance(on_exchange_headers_any)
                )
                self.ioengine.ioloop.add_callback(
                    self.__rabbitmq_exchange_topic,
                    future=self.ioengine.future_instance(on_exchange_topic)
                )
                self.ioengine.ioloop.add_callback(self.__rabbitmq_exchange_headers_ae)
                self.ioengine.ioloop.add_callback(self.__rabbitmq_exchange_headers_any_ae)
            return True

        try:
            assert self.rabbitmq
        except AssertionError:
            self.ioengine.ioloop.add_callback(self.__rabbitmq,future=self.ioengine.future_instance(on_rabbitmq))
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(self.__rabbitmq_channel,future=self.ioengine.future_instance(on_channel))
        return True

    def stop(self):
        return True
