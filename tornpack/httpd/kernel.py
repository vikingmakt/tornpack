from tornpack.actor.base import Base
from tornpack.options import options
from .engine import HTTPD

__all__ = ['Kernel']

class Kernel(Base):
    __servers = None

    @property
    def servers(self):
        try:
            assert self.__servers
        except AssertionError:
            self.__servers = {}
        except:
            raise
        return self.__servers

    def server(self,name,future,port=options.tornpack_httpd_port):
        try:
            assert name in self.servers
        except AssertionError:
            self.servers[name] = HTTPD()
        except:
            raise

        future.set_result(self.servers[name])
        return True
