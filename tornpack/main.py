from tornado.autoreload import add_reload_hook, start as AutoreloadStart
from tornpack.actor.base import Base
from tornpack.httpd import Kernel as HTTPD
import tornpack.njord
from tornpack.options import options
from tornpack.rabbitmq import RabbitMQ
from tornpack.raven import Raven

__all__ = ['Main']

class Main(Base):
    def init(self):
        self.setup_autoreload()
        self.setup()
        self.gen()
        return True

    def gen(self):       
        while self.run():
            pass

        Raven.info('stop')
        return True

    def rabbitmq_start(self):
        def on_open(result):
            Raven.info('tornpack:rabbitmq:start')
            return True
        
        RabbitMQ.engine_open(name=options.tornpack_rabbitmq_name,
                             url=options.tornpack_rabbitmq_url,
                             future=self.ioengine.future_instance(on_open))
        return True
    
    def run(self):
        self.setup()
        
        try:
            self.ioengine.ioloop.add_callback(self.start)
            self.ioengine.ioloop.start()
        except KeyboardInterrupt:
            self.stop()
            self.ioengine.ioloop.stop()
            return False
        except:
            pass
        return True

    def setup(self):
        pass

    def setup_autoreload(self):
        try:
            assert options.autoreload
            AutoreloadStart()
            add_reload_hook(self.stop)
        except (AssertionError,AttributeError,NameError):
            pass
        except:
            raise
        return True

    def start(self):
        self.ioengine.ioloop.add_callback(self.rabbitmq_start)
        return True

    def stop(self):
        pass
