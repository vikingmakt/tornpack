from base64 import b64decode,b64encode
from hashlib import sha1
from tornpack.actor.base import Base
from tornpack.njord import Njord
from tornpack.options import options
from tornpack.parser.json import jsonify
from .pb import Request as RequestPB,Response as ResponsePB

__all__ = ['Request']

class Request(Base):
    __etag = None

    @property
    def etag(self):
        try:
            assert self.__etag
        except AssertionError:
            self.__etag = sha1('%s:%s<%s>' % (self.name,self.ioengine.uuid4,self.ioengine.uuid4)).hexdigest()
        except:
            raise
        return self.__etag

    @property
    def name(self):
        return 'njord_http'

    @staticmethod
    def __method__(name):
        return RequestPB.METHOD.Value(name)

    def __body_decode__(self,arg):
        try:
            return b64decode(arg)
        except:
            pass
        return None

    def __body_encode__(self,arg):
        try:
            return b64encode(arg)
        except:
            pass
        return None

    def ask(self,msg,request,future):
        def on_ask(result):
            try:
                assert result.result()
            except AssertionError:
                future.set_result(False)
            except:
                raise
            else:
                def on_decode(result):
                    future.set_result({
                        'code':result.result().code,
                        'status_code':result.result().status_code,
                        'body':self.__body_decode__(result.result().body)
                    })
                    return True

                self.__protobuf_decode__(
                    result.result()['body'],
                    ResponsePB(),
                    self.ioengine.future_instance(on_decode)
                )
            return True

        Njord.publish(
            name=self.name,
            body=request,
            future=self.ioengine.future_instance(on_ask)
        )
        return True

    def headers_parser(self,msg,future):
        try:
            msg['headers'] = jsonify(msg['headers'])
            assert msg['headers']
        except AssertionError:
            del msg['headers']
        except KeyError:
            pass
        except:
            raise

        self.ioengine.ioloop.add_callback(
            self.parse_method,
            msg=msg,
            future=future
        )
        return True

    def make(self,url,future,**kwargs):
        kwargs['url'] = url
        self.ioengine.ioloop.add_callback(
            self.headers_parser,
            future=future,
            msg=kwargs
        )
        return True

    def parse_conn_timeout(self,msg,future):
        try:
            assert isinstance(msg['conn_timeout'],(float,int))
            assert msg['conn_timeout'] > 0
        except AssertionError:
            del msg['conn_timeout']
        except KeyError:
            pass
        except:
            pass

        self.ioengine.ioloop.add_callback(
            self.parse_req_timeout,
            msg=msg,
            future=future
        )
        return True

    def parse_method(self,msg,future):
        try:
            assert isinstance(msg['method'],(str,unicode))
            msg['method'] = Request.__method__(msg['method'])
        except (AssertionError,KeyError,ValueError):
            pass
        except:
            raise

        self.ioengine.ioloop.add_callback(
            self.parse_conn_timeout,
            msg=msg,
            future=future
        )
        return True

    def parse_req_timeout(self,msg,future):
        try:
            assert isinstance(msg['req_timeout'],(float,int))
            assert msg['req_timeout'] > 0
        except AssertionError:
            del msg['req_timeout']
        except KeyError:
            pass
        except:
            pass

        self.ioengine.ioloop.add_callback(
            self.payload,
            msg=msg,
            future=future
        )
        return True

    def payload(self,msg,future):
        try:
            msg['body'] = self.__body_encode__(msg['body']) or ''
        except:
            pass

        self.ioengine.ioloop.add_callback(
            self.ask,
            msg=msg,
            request=RequestPB(uid=self.etag,**msg),
            future=future
        )
        return True
