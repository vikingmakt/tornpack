from tornpack.actor.base import Base
from tornpack.njord import Njord
from tornpack.njord.mongodb.cursor.handler import Handler as Cursor
from tornpack.njord.mongodb.static import mongodb_path as MDBPATH
from tornpack.options import options
from tornpack.parser.json import dictfy,jsonify

__all__ = ['Aggregate']

class Aggregate(Base):
    @property
    def name(self):
        return 'njord_mongodb_aggregate'

    @property
    def service(self):
        return 'aggregate'

    def mongodb_path(self,env,db,collection):
        return MDBPATH(env,db,collection,self.service)

    def run(self,env,db,collection,query,future):
        def on_ask(result):
            try:
                body = dictfy(result.result()['body'])
                assert body
                future.set_result({'cursor':Cursor(body['service'],body['cursor']),'doc':body['doc']})
            except (AssertionError,AttributeError):
                future.set_result(False)
            except:
                raise
            return True

        Njord.publish(
            name=self.name,
            future=self.ioengine.future_instance(on_ask),
            body=jsonify(query),
            headers={
                'mongodb_path':self.mongodb_path(
                    env=env,
                    db=db,
                    collection=collection
                )
            }
        )
        return True
