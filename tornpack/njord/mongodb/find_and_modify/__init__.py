from tornpack.actor.base import Base
from tornpack.njord import Njord
from tornpack.njord.mongodb.static import mongodb_path as MDBPATH
from tornpack.options import options
from tornpack.parser.json import dictfy,jsonify

__all__ = ['FindAndModify']

class FindAndModify(Base):
    @property
    def name(self):
        return 'njord_mongodb_find_and_modify'

    @property
    def service(self):
        return 'find_and_modify'

    def mongodb_path(self,env,db,collection):
        return MDBPATH(env,db,collection,self.service)

    def run(self,env,db,collection,query,document,op,future,fields=None,skip=0,sort=None,new=False,upsert=False):
        try:
            assert op in ('update','remove')
        except AssertionError:
            raise TypeError('op must be "update" or "remove"')
        except:
            raise

        def on_ask(result):
            future.set_result(dictfy(result.result()['body']))
            return True

        Njord.publish(
            name=self.name,
            future=self.ioengine.future_instance(on_ask),
            body={
                'query':query,
                'document':document,
                'op':op
            },
            headers={
                'mongodb_path':self.mongodb_path(
                    env=env,
                    db=db,
                    collection=collection
                ),
                'fields':fields,
                'skip':skip,
                'sort':sort,
                'new':new,
                'upsert':upsert
            }
        )
        return True
