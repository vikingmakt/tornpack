from tornpack.actor.base import Base
from tornpack.njord import Njord
from tornpack.njord.mongodb.static import mongodb_path as MDBPATH
from tornpack.options import options
from tornpack.parser.json import dictfy

__all__ = ['Update']

class Update(Base):
    @property
    def name(self):
        return 'njord_mongodb_update'

    @property
    def service(self):
        return 'update'

    def mongodb_path(self,env,db,collection):
        return MDBPATH(env,db,collection,self.service)

    def run(self,env,db,collection,query,document,future,multi=False,upsert=False):
        def on_ask(result):
            future.set_result(dictfy(result.result()['body']))
            return True

        Njord.publish(
            name=self.name,
            future=self.ioengine.future_instance(on_ask),
            body={'query':query,'document':document},
            headers={
                'mongodb_path':self.mongodb_path(
                    env=env,
                    db=db,
                    collection=collection
                ),
                'upsert':upsert,
                'multi':multi
            }
        )
        return True
