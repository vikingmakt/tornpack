__all__ = [
    'DecodeException',
    'EncodeException',
    'ExecutionError',
    'MySQLException',
    'QueryInvalid'
]

class MySQLException(Exception):
    pass

class DecodeException(MySQLException):
    pass

class EncodeException(MySQLException):
    pass

class QueryError(MySQLException):
    pass

class QueryInvalid(MySQLException):
    pass

class ExecutionError(MySQLException):
    pass
