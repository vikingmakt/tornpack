from tornpack.actor.base import Base
from tornpack.njord import Njord
from tornpack.njord.mysql.exceptions import DecodeException,EncodeException,ExecutionError
from tornpack.njord.mysql.static import mysql_path as MySQLPATH
from tornpack.options import options
from tornpack.parser.json import dictfy
from select_one_pb2 import SelectOne as PBSelectOne,SelectOneResponse as PBSelectOneResponse

__all__ = ['SelectOne']

class SelectOne(Base):
    __op_codes = None

    @property
    def codes(self):
        return options.tornpack_njord_mysql['codes']['select_one']

    @property
    def name(self):
        return 'njord_mysql_select_one'

    @property
    def op_codes(self):
        try:
            assert self.__op_codes
        except AssertionError:
            self.__op_codes = {
                self.codes['decode_error']:self.__op_decode_error__,
                self.codes['execution_error']:self.__op_execution_error__,
                self.codes['nok']:self.__op_nok__,
                self.codes['ok']:self.__op_ok__
            }
        except:
            raise
        return self.__op_codes

    @property
    def service(self):
        return 'select_one'

    def __op_decode_error__(self,body,future):
        future.set_exception(DecodeException())
        return True

    def __op_execution_error__(self,body,future):
        future.set_exception(ExecutionError(body.error.code,body.error.text))
        return True

    def __op_nok__(self,body,future):
        future.set_result(False)
        return True

    def __op_ok__(self,body,future):
        future.set_result(dictfy(body.payload))
        return True

    def __prepare_proto__(self,env,cond,having,join,fields,group,order):
        try:
            proto = PBSelectOne(
                env=env
            )
            self.__proto_set_optional__('cond',cond,proto)
            self.__proto_set_optional__('having',having,proto)
            self.__proto_set_list__('fields',fields,proto)
            self.__proto_set_list__('group',group,proto)

            if join:
                for arg in join:
                    proto.join.add(**arg)

            if order:
                for arg in order:
                    proto.order.add(**arg)
        except TypeError:
            raise EncodeException
        except:
            raise
        return proto

    def __proto_set_list__(self,k,v,proto):
        try:
            assert v
            assert isinstance(v,(list,tuple))
        except AssertionError:
            pass
        except:
            raise
        else:
            getattr(proto,k).extend(v)
        return proto

    def __proto_set_optional__(self,k,v,proto):
        try:
            assert v is None
        except AssertionError:
            setattr(proto,k,v)
        except:
            raise
        return proto

    @staticmethod
    def parse_join_type(name):
        return PBSelectOne.Join.Type.Value(name)

    @staticmethod
    def parse_order_direction(name):
        return PBSelectOne.Order.Direction.Value(name)

    def mysql_path(self,env,db,table):
        return MySQLPATH(env,db,table,self.service)

    def run(self,env,db,table,future,cond=None,having=None,join=None,fields=None,group=None,order=None):
        def on_decode(result):
            self.ioengine.ioloop.add_callback(
                self.op_codes[result.result().code],
                body=result.result(),
                future=future
            )
            return True

        def on_ask(result):
            try:
                assert result.result()
            except AssertionError:
                pass
            except:
                raise
            else:
                self.ioengine.ioloop.add_callback(
                    self.__protobuf_decode__,
                    result.result()['body'],
                    PBSelectOneResponse(),
                    self.ioengine.future_instance(on_decode)
                )
            return True

        Njord.publish(
            name=self.name,
            body=self.__prepare_proto__(
                env=env,
                cond=cond,
                having=having,
                join=join,
                fields=fields,
                group=group,
                order=order
            ),
            future=self.ioengine.future_instance(on_ask),
            headers={
                'mysql_path':self.mysql_path(
                    env=env,
                    db=db,
                    table=table
                ),
                'service':options.tornpack_njord_mysql_service_name,
                'db':db,
                'env':env,
                'table':table,
                'action':self.service
            }
        )
        return True
