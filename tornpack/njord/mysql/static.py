from tornpack.options import options

__all__ = [
    'mysql_path'
]

def mysql_path(env,db,table,service):
    return '%s.%s.%s.%s' % (env,db,table,service)
