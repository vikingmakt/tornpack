from tornpack.actor.base import Base
from tornpack.njord import Njord
from tornpack.njord.mysql.exceptions import DecodeException,ExecutionError
from tornpack.njord.mysql.static import mysql_path as MySQLPATH
from tornpack.options import options
from tornpack.parser.json import jsonify
from .update_pb2 import Update as PBUpdate,UpdateResponse as PBUpdateResponse

__all__ = ['Update']

class Update(Base):
    __op_codes = None

    @property
    def codes(self):
        return options.tornpack_njord_mysql['codes']['update']

    @property
    def name(self):
        return 'njord_mysql_update'

    @property
    def op_codes(self):
        try:
            assert self.__op_codes
        except AssertionError:
            self.__op_codes = {
                self.codes['decode_error']:self.__op_decode_error__,
                self.codes['execution_error']:self.__op_execution_error__,
                self.codes['nok']:self.__op_nok__,
                self.codes['ok']:self.__op_ok__
            }
        except:
            raise
        return self.__op_codes

    @property
    def service(self):
        return 'update'

    def __op_decode_error__(self,body,future):
        future.set_exception(DecodeException())
        return True

    def __op_execution_error__(self,body,future):
        future.set_exception(ExecutionError(body.error.code,body.error.text))
        return True

    def __op_nok__(self,body,future):
        future.set_result(False)
        return True

    def __op_ok__(self,body,future):
        future.set_result(True)
        return True

    def mysql_path(self,env,db,table):
        return MySQLPATH(env,db,table,self.service)

    def run(self,env,db,table,cond,document,future):
        def on_decode(result):
            self.ioengine.ioloop.add_callback(
                self.op_codes[result.result().code],
                body=result.result(),
                future=future
            )
            return True

        def on_ask(result):
            try:
                assert result.result()
            except AssertionError:
                pass
            except:
                raise
            else:
                self.ioengine.ioloop.add_callback(
                    self.__protobuf_decode__,
                    result.result()['body'],
                    PBUpdateResponse(),
                    self.ioengine.future_instance(on_decode)
                )
            return True

        Njord.publish(
            name=self.name,
            body=PBUpdate(
                env=env,
                cond=cond,
                payload=jsonify(document)
            ),
            future=self.ioengine.future_instance(on_ask),
            headers={
                'mysql_path':self.mysql_path(
                    env=env,
                    db=db,
                    table=table
                ),
                'service':options.tornpack_njord_mysql_service_name,
                'db':db,
                'env':env,
                'table':table,
                'action':self.service
            }
        )
        return True
