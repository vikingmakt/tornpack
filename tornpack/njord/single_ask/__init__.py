from pika import BasicProperties
from tornpack.actor.rabbitmq.connection import Connection
from tornpack.options import options

__all__ = ['SingleAsk']

class SingleAsk(Connection):
    __dogtag = None
    __futures = None
    __queue = None

    @property
    def dogtag(self):
        try:
            assert self.__dogtag
        except AssertionError:
            self.__dogtag = self.ioengine.uuid4
        except:
            raise
        return self.__dogtag

    @property
    def queue(self):
        try:
            assert self.__queue
        except AssertionError:
            self.__queue = self.ioengine.uuid4
        except:
            raise
        return self.__queue

    def __ask__(self,future,body='',exchange=None,routing_key='',headers={},*args,**kwargs):
        try:
            assert self.is_ready
        except AssertionError:
            def on(_):
                self.ioengine.ioloop.add_callback(
                    self.__ask__,
                    future=future,
                    body=body,
                    exchange=exchange,
                    routing_key=routing_key,
                    headers=headers
                )
                return True

            self.promises.append(self.ioengine.future_instance(on))
            return True
        except:
            raise

        etag = self.ioengine.uuid4
        self.__futures[etag] = future
        headers['etag'] = etag
        headers['__ask__'] = True
        headers['dogtag'] = self.dogtag

        self.channel.basic_publish(
            body=body,
            exchange=exchange or options.tornpack_rabbitmq_exchange['default'],
            routing_key=routing_key,
            properties=BasicProperties(headers=headers)
        )
        return True

    def __channel__(self):
        def on(_):
            self.channel = _.result()
            self.ioengine.ioloop.add_callback(self.__exchange_declare__)
            return True

        self.rabbitmq_connection.channel('single_ask',self.ioengine.future_instance(on))
        return True

    def __consumer__(self):
        self.channel.basic_consume(
            consumer_callback=self.on_msg,
            queue=self.queue
        )
        return True

    def __exchange_declare__(self):
        def on_declare(*args):
            self.channel.exchange_bind(
                callback=None,
                destination='single_ask',
                source='tornpack',
                routing_key='#'
            )
            self.ioengine.ioloop.add_callback(self.__queue_declare__)
            return True

        self.channel.exchange_declare(
            callback=on_declare,
            exchange='single_ask',
            exchange_type='headers',
            arguments={
                'x-match':'any'
            }
        )
        return True

    def __init__(self):
        def on(_):
            self.ioengine.ioloop.add_callback(self.__channel__)
            return True

        self.__futures = {}
        self.ioengine.ioloop.add_callback(
            self.__rabbitmq_connection__,
            self.ioengine.future_instance(on)
        )

    def __queue_bind__(self):
        def on_bind(*args):
            self.is_ready = True
            return True

        self.channel.queue_bind(
            callback=on_bind,
            exchange='single_ask',
            queue=self.queue,
            routing_key='',
            arguments={
                'dogtag':self.dogtag
            }
        )
        return True

    def __queue_declare__(self):
        def on_declare(*args):
            self.ioengine.ioloop.add_callback(self.__queue_bind__)
            self.ioengine.ioloop.add_callback(self.__consumer__)
            return True

        self.channel.queue_declare(
            auto_delete=True,
            callback=on_declare,
            durable=False,
            exclusive=True,
            queue=self.queue
        )
        return True

    def on_msg(self,channel,method,properties,body):
        try:
            assert not properties.headers.get('__ask__')
            self.__futures.pop(properties.headers['etag']).set_result({
                'body':body,
                'channel':channel,
                'properties':properties
            })
        except:
            pass

        channel.basic_ack(method.delivery_tag)
        return True
