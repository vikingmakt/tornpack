from base64 import b64decode,b64encode
from tornpack.actor.rabbitmq.static import routing_key as RK
from tornpack.njord.single_ask import SingleAsk
from tornpack.options import options
from tornpack.parser.json import dictfy,jsonify

__all__ = ['Execute']

class Execute(SingleAsk):
    __op_codes = None

    @property
    def codes(self):
        return options.tornpack_njord_soap['codes']['execute']

    @property
    def name(self):
        return 'njord_soap_execute'

    @property
    def op_codes(self):
        try:
            assert self.__op_codes
        except AssertionError:
            self.__op_codes = {
                self.codes['client_error']:self.__op_client_error__,
                self.codes['method_error']:self.__op_method_error__,
                self.codes['method_not_exists']:self.__op_method_not_exists__,
                self.codes['ok']:self.__op_ok__
            }
        except:
            raise
        return self.__op_codes

    def __op_client_error__(self,request,response,return_time,future):
        future.set_exception(ClientError(request['url'],response['responseMsg'],response.get('time')))
        return True

    def __op_method_error__(self,request,response,return_time,future):
        future.set_exception(MethodError(request['url'],request['method'],response['responseMsg'],response.get('time')))
        return True

    def __op_method_not_exists__(self,request,response,return_time,future):
        future.set_exception(MethodNotExists(request['url'],request['method']))
        return True

    def __op_ok__(self,request,response,return_time,future):
        try:
            assert return_time
        except AssertionError:
            future.set_result(response['responseMsg'])
        except:
            raise
        else:
            future.set_result({
                'data':response['responseMsg'],
                'time':response.get('time')
            })
        return True

    def ask(self,payload,headers,return_time,future):
        def on_ask(result):
            try:
                assert result.result()
            except AssertionError:
                pass
            except:
                raise
            else:
                self.ioengine.ioloop.add_callback(
                    self.op_codes[result.result()['properties'].headers['code']],
                    response=dictfy(b64decode(result.result()['body'])),
                    request=payload,
                    return_time=return_time,
                    future=future
                )
            return True

        self.ioengine.ioloop.add_callback(
            self.__ask__,
            routing_key=RK(self.name),
            future=self.ioengine.future_instance(on_ask),
            body=b64encode(jsonify(payload)),
            headers=headers
        )
        return True

    def payload_args(self,payload,args,**kwargs):
        try:
            assert args
        except AssertionError:
            pass
        except:
            raise
        else:
            payload['args'] = args

        self.ioengine.ioloop.add_callback(
            self.payload_auth,
            payload=payload,
            **kwargs
        )
        return True

    def payload_auth(self,payload,username,password,**kwargs):
        try:
            assert username
            assert password
        except AssertionError:
            pass
        except:
            raise
        else:
            payload['username'] = username
            payload['password'] = password

        self.ioengine.ioloop.add_callback(
            self.payload_env,
            payload=payload,
            **kwargs
        )
        return True

    def payload_env(self,payload,env,**kwargs):
        try:
            assert env
        except AssertionError:
            env = options.tornpack_njord_soap['env']['default']
        except:
            raise

        self.ioengine.ioloop.add_callback(
            self.validate_payload,
            payload=payload,
            headers={
                'soap_env': env
            },
            **kwargs
        )
        return True

    def payload_timeout(self,payload,timeout,**kwargs):
        try:
            assert timeout
        except AssertionError:
            pass
        except:
            raise
        else:
            payload['timeout'] = timeout

        self.ioengine.ioloop.add_callback(
            self.payload_args,
            payload=payload,
            **kwargs
        )
        return True

    def run(self,url,method,future,env=None,timeout=None,username=None,password=None,args=None,return_time=False):
        try:
            assert isinstance(url,(str,unicode))
            assert isinstance(method,(str,unicode))
        except AssertionError:
            future.set_exception(TypeError())
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(
                self.payload_timeout,
                payload={
                    'url':url,
                    'method':method
                },
                env=env,
                args=args,
                username=username,
                password=password,
                timeout=timeout,
                return_time=return_time,
                future=future
            )
        return True

    def validate_payload(self,payload,headers,return_time,future):
        try:
            assert isinstance(payload['url'],(str,unicode))
            assert isinstance(payload['method'],(str,unicode))
            assert isinstance(headers['soap_env'],(str,unicode))
            assert 'username' not in payload or isinstance(payload['username'],(str,unicode))
            assert 'password' not in payload or isinstance(payload['password'],(str,unicode))
            assert 'timeout' not in payload or isinstance(payload['timeout'],int)
            assert 'args' not in payload or isinstance(payload['args'],dict)
        except AssertionError:
            future.set_exception(TypeError())
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(
                self.ask,
                payload=payload,
                headers=headers,
                return_time=return_time,
                future=future
            )
        return True

class SOAPException(Exception):
    pass

class ClientError(SOAPException):
    def __str__(self):
        return 'A error occurred in WebService %s : %s' % (self.url,self.message)

    def __init__(self,url,msg,time):
        self.url = url
        self.time = time
        super(SOAPException,self).__init__('%s <time:%s>' % (msg,time))

class MethodError(SOAPException):
    def __str__(self):
        return 'A error occurred in WebService %s::%s : %s' % (self.url,self.method,self.message)

    def __init__(self,url,method,msg,time):
        self.url = url
        self.method = method
        self.time = time
        super(SOAPException,self).__init__('%s <time:%s>' % (msg,time))

class MethodNotExists(SOAPException):
    def __str__(self):
        return 'Method %s does not exists in WebService %s' % (self.method,self.url)

    def __init__(self,url,method):
        self.url = url
        self.method = method
