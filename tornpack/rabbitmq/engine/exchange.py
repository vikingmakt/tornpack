from tornpack.actor.base import Base

__all__ = ['Exchange']

class Exchange(Base):
    def bind(self,channel,destination,source,routing_key,arguments=None,future=None):
        def on_bind(result):
            try:
                future.set_result(result)
            except AttributeError:
                pass
            except:
                raise
            return True

        channel.exchange_bind(destination=destination,source=source,routing_key=routing_key,arguments=arguments,callback=on_bind)
        return True

    def declare(self,channel,exchange,exchange_type='direct',passive=False,durable=False,auto_delete=False,internal=False,arguments=None,future=None):
        def on_declare(result):
            try:
                future.set_result(result)
            except AttributeError:
                pass
            except:
                raise
            return True

        channel.exchange_declare(exchange=exchange,exchange_type=exchange_type,passive=passive,
                                 durable=durable,auto_delete=auto_delete,internal=internal,
                                 arguments=arguments,callback=on_declare)
        return True

    def delete(self,channel,exchange,if_unused=False,future=None):
        def on_delete(result):
            try:
                future.set_result(result)
            except AttributeError:
                pass
            except:
                raise
            return True

        channel.exchange_delete(exchange=exchange,if_unused=if_unused,callback=on_delete)
        return True

    def unbind(self,channel,destination,source,routing_key,arguments=None,future=None):
        def on_unbind(result):
            try:
                future.set_result(result)
            except AttributeError:
                pass
            except:
                raise
            return True

        channel.exchange_unbind(exchange=exchange,if_unused=if_unused,callback=on_unbind)
        return True

Exchange = Exchange()
