from hashlib import sha1
from tornpack.options import options

__all__ = ['ko']

def ko(o,k):
    return sha1(options.tornpack_dhtdb_encode['ko'] % (o,k)).hexdigest()
