from tornpack.actor.base import Base
from .actions import actions as ACTIONS

__all__ = ['Uprofile']

class Uprofile(Base):
    @property
    def actions(self):
        return ACTIONS

    def on_msg(self,msg,io):
        try:
            assert msg['header']['method'] in self.actions
        except (AssertionError,KeyError):
            self.ioengine.ioloop.add_callback(io.disconnect)
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(self.actions[msg['header']['method']],msg=msg,io=io)
        return True
