from hashlib import sha1
from tornpack.acl.auth import Auth as ACL
from tornpack.actor.rabbitmq import SimpleAsk
from tornpack.actor.rabbitmq.static import routing_key
from tornpack.options import options
from tornpack.parser.json import dictfy,jsonify

__all__ = ['Set']

class Set(SimpleAsk):
    def __acl_error__(self,io,etag):
        io.error(
            etag=etag,
            code=options.tornpack_raid_codes['acl']['unauthorized']
        )
        return True

    def __acl_user__(self,io):
        try:
            assert io.user
        except AssertionError:
            return acl_app(io.app)
        except:
            raise
        return acl_user(
            io.app,
            io.user,
            io.user_type
        )

    def ask(self,msg,io):
        def on_tell(result):
            try:
                io.push({
                    'body':dictfy(result.result()['body']),
                    'header':{
                        'etag':msg['header']['etag'],
                        'code':result.result()['properties'].headers['code']
                    }
                })
            except KeyError:
                pass
            except:
                raise
            return True

        self.ioengine.ioloop.add_callback(
            self.__ask__,
            future=self.ioengine.future_instance(on_tell),
            body=jsonify({
                'g':msg['body']['g'],
                'k':msg['body']['k'],
                'v':msg['body']['v']
            }),
            routing_key=routing_key(options.tornpack_uprofile_rabbitmq['services']['acl']['set'])
        )
        return True

    def check_acl(self,msg,io):
        try:
            assert io.user
        except AssertionError:
            self.__acl_error__(io,msg['header']['etag'])
        except:
            raise
        else:
            def on_check(result):
                try:
                    assert result.result()
                except AssertionError:
                    self.__acl_error__(io,msg['header']['etag'])
                except:
                    raise
                else:
                    self.ioengine.ioloop.add_callback(self.ask,msg=msg,io=io)
                return True

            ACL(
                group=sha1('admin_%s' % acl_app(io.app)).hexdigest(),
                user=self.__acl_user__(io),
                spec='w',
                future=self.ioengine.future_instance(on_check)
            )
        return True

    def on_msg(self,msg,io):
        try:
            assert msg['body']['g']
            assert msg['body']['k']
            assert msg['body']['v'] in ('r','w')
        except:
            io.error(
                etag=msg['header']['etag'],
                code=options.tornpack_raid_codes['payload']['invalid']
            )
        else:
            self.ioengine.ioloop.add_callback(self.check_acl,msg=msg,io=io)
        return True
