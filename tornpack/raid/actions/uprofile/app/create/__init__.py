from tornpack.actor.rabbitmq import SimpleAsk
from tornpack.actor.rabbitmq.static import routing_key as RK
from tornpack.options import options
from tornpack.parser.json import dictfy,jsonify

__all__ = ['Create']

class Create(SimpleAsk):
    def ask(self,msg,io):
        def on_ask(result):
            io.push({
                'body':dictfy(result.result()['body']),
                'header':{
                    'etag':msg['header']['etag'],
                    'code':result.result()['properties'].headers['code']
                }
            })
            return True

        self.ioengine.ioloop.add_callback(
            self.__ask__,
            future=self.ioengine.future_instance(on_ask),
            body=jsonify(msg['body']),
            routing_key=RK(options.tornpack_uprofile_rabbitmq['services']['app']['create'])
        )
        return True

    def on_msg(self,msg,io):
        try:
            assert msg['header']['etag']
            assert msg['body']['name']
        except (AssertionError,KeyError,TypeError):
            io.error(
                etag=msg['header']['etag'],
                code=options.tornpack_raid_codes['payload']['invalid']
            )
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(self.ask,msg=msg,io=io)
        return True
