from tornpack.actor.rabbitmq import SimpleAsk
from tornpack.actor.rabbitmq.static import routing_key
from tornpack.options import options
from tornpack.parser.json import dictfy,jsonify

__all__ = ['Get']

class Get(SimpleAsk):
    def ask(self,msg,io):
        def on_tell(result):
            try:
                io.push({
                    'body':dictfy(result.result()['body']),
                    'header':{
                        'etag':msg['header']['etag'],
                        'code':result.result()['properties'].headers['code']
                    }
                })
            except KeyError:
                pass
            except:
                raise
            return True

        self.ioengine.ioloop.add_callback(
            self.__ask__,
            future=self.ioengine.future_instance(on_tell),
            body=jsonify(msg['body']),
            routing_key=routing_key(options.tornpack_uprofile_rabbitmq['services']['corp']['get'])
        )
        return True

    def on_msg(self,msg,io):
        try:
            assert msg['body']['_id']
        except:
            io.error(
                etag=msg['header']['etag'],
                code=options.tornpack_raid_codes['payload']['invalid']
            )
        else:
            self.ioengine.ioloop.add_callback(self.ask,msg=msg,io=io)
        return True
