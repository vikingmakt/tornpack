from tornpack.actor.rabbitmq import SimpleAsk
from tornpack.options import options
from tornpack.actor.rabbitmq.static import routing_key
from tornpack.options import options
from tornpack.parser.json import dictfy,jsonify

__all__ = ['Create']

class Create(SimpleAsk):
    def __acl_error__(self,io,etag):
        io.error(
            etag=etag,
            code=options.tornpack_raid_codes['acl']['unauthorized']
        )
        return True

    def app_auth(self,msg,io):
        try:
            assert io.app
        except AssertionError:
            self.__acl_error__(io,msg['header']['etag'])
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(self.ask,msg=msg,io=io)
        return True

    def ask(self,msg,io):
        def on_tell(result):
            try:
                io.push({
                    'body':dictfy(result.result()['body']),
                    'header':{
                        'etag':msg['header']['etag'],
                        'code':result.result()['properties'].headers['code']
                    }
                })
            except KeyError:
                pass
            except:
                raise
            return True

        self.ioengine.ioloop.add_callback(
            self.__ask__,
            future=self.ioengine.future_instance(on_tell),
            body=jsonify(msg['body']),
            routing_key=routing_key(options.tornpack_uprofile_rabbitmq['services']['user']['create'])
        )
        return True

    def on_msg(self,msg,io):
        try:
            assert msg['body']['user']
            assert msg['body']['type'] in options.tornpack_uprofile_user_types
        except:
            io.error(
                etag=msg['header']['etag'],
                code=options.tornpack_raid_codes['payload']['invalid']
            )
        else:
            self.ioengine.ioloop.add_callback(self.app_auth,msg=msg,io=io)
        return True
