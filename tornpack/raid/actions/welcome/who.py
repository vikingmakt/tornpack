from datetime import datetime
from tornpack.actor.base import Base
from tornpack.options import options
from tornpack.parser.date import datetime2timestamp

__all__ = ['Who']

class Who(Base):
    @property
    def hi_msg(self):
        return {
            'header':{
                'action':'welcome',
                'method':'who',
                'datetime':datetime2timestamp(datetime.utcnow()),
                '__sysdate__':datetime2timestamp(datetime.utcnow())
            }
        }

    def init(self,msg,io):
        io.push(self.hi_msg)
        return True
