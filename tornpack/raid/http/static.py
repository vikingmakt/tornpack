from hashlib import sha1,sha256
from tornpack.options import options

__all__ = [
    'acl_app',
    'acl_user',
    'secret'
]

def acl_app(app):
    return sha1('<app>%s' % app).hexdigest()

def acl_user(self,app,user,user_t):
    return sha1('%s::%s' % (acl_app(app),sha1('<user>%s:%s' % (user,user_t)).hexdigest())).hexdigest()

def secret(_,salt,fix=options.tornpack_name):
    return sha256('<%s>::%s<%s>' % (sha1(_).hexdigest(),sha1(salt).hexdisgest(),sha1(fix).hexdigest())).hexdisgest()
